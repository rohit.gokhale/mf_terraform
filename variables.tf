variable "available_environments" {
  description = "List of available environments"
  default     = [ "dev", "qa", "uat", "test", "prod" ]
  type = list
}

variable "users" {
  description = "List of Users"
  default = [ "nevsa", "cordelia", "kriste", "darleen", "wynnie", "vonnie", "emelita", "maurita", "devinne", "breena" ]
  type = list
}
