# MF_terraform

Matchesfashion 
==============

Platform Engineering Technical Test Solution
---------------------------------------------


Please export AWS access credentials and region


export AWS_ACCESS_KEY_ID="key_id"

export AWS_SECRET_ACCESS_KEY="secret"

export AWS_DEFAULT_REGION="eu-west-2"

`terraform init`

`terraform plan`

`terraform apply`
