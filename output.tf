output "iam_access_key_ids" {
  description = "The access key ID"
  value = join ( "\n", [ for account_keys in aws_iam_access_key.service_account_keys.* : "${account_keys.user}, ${account_keys.id}, ${account_keys.secret}" ] )
}
