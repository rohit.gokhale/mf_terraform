resource "aws_iam_user" "service_account" {
  name = "${element(var.users, (count.index % length(var.users)))}-${element(var.available_environments, floor(count.index / length(var.users)))}"
  count = "${length(var.users)*length(var.available_environments)}"
}

resource "aws_iam_access_key" "service_account_keys" {
  user = "${element(aws_iam_user.service_account.*.name, count.index)}"
  count = "${length(var.users)*length(var.available_environments)}"
}
